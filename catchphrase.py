import time
import random
import Tkinter as Tki
import tkMessageBox as tkMB
from scipy import stats
from PIL import Image
from PIL import ImageTk
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.backends.backend_tkagg import NavigationToolbar2TkAgg
matplotlib.use('TkAgg')

root = Tki.Tk()
root.title("Catchphrase")
root.attributes('-fullscreen', True)
menu_frame = Tki.Frame(root)
game_frame = Tki.Frame(root)
settings_frame = Tki.Frame(root)
graph_frame = Tki.Frame(settings_frame)
min_duration = Tki.DoubleVar()
max_duration = Tki.DoubleVar()
mean_duration = Tki.DoubleVar()
sd_duration = Tki.DoubleVar()
min_duration.set(0)
max_duration.set(60)
mean_duration.set((min_duration.get() + max_duration.get()) / 2)
sd_duration.set(1)


def show_graph():
    global canvas, toolbar
    fig, ax = plt.subplots()
    x = np.arange(min_duration.get(), max_duration.get(), 0.1)
    ax.set_xlabel("Duration")
    ax.set_ylabel("Probability Density")
    plt.title("Chosen distribution")
    if min_duration.get() == max_duration.get():
        ax.plot([min_duration.get()] * 2, [0, 1])
    else:
        ax.plot(x, stats.truncnorm.pdf(x,
                                       min_duration.get() -
                                       mean_duration.get(),
                                       max_duration.get() -
                                       mean_duration.get(),
                                       loc=mean_duration.get(),
                                       scale=sd_duration.get()))
    canvas = FigureCanvasTkAgg(fig, master=graph_frame)
    canvas.show()
    canvas.get_tk_widget().pack(side=Tki.TOP, fill=Tki.BOTH, expand=1)
    toolbar = NavigationToolbar2TkAgg(canvas, graph_frame)
    toolbar.update()
    canvas._tkcanvas.pack(side=Tki.TOP, fill=Tki.BOTH, expand=1)


show_graph()


def update_text(self):
    if text:
        file = open("clues.txt", "a")
        file.write(text.get() + "\n")
        file.close()
        txt_entry.delete(0, "end")


def update_sliders(x, y, z):
    max_slider.configure(from_=min_duration.get(),
                         to_=min_duration.get() + 200)
    plt.close()
    canvas.get_tk_widget().pack_forget()
    canvas._tkcanvas.pack_forget()
    toolbar.pack_forget()
    show_graph()


def update_distribution(x, y, z):
    if distribution.get() == "Normal":
        mean_slider.grid(row=3, column=1, sticky="W")
        mean_slider_label.grid(row=3, column=0, sticky="S")
        sd_slider.grid(row=4, column=1, sticky="W")
        sd_slider_label.grid(row=4, column=0, sticky="S")
        txt_label.grid(row=5, column=0)
        txt_button.grid(row=6, columnspan=2)
        txt_entry.grid(row=5, column=1)
        graph_frame.grid(column=2, rowspan=5)
        canvas.get_tk_widget().pack_forget()
        canvas._tkcanvas.pack_forget()
        toolbar.pack_forget()
        show_graph()
    else:
        mean_slider.grid_forget()
        mean_slider_label.grid_forget()
        sd_slider.grid_forget()
        sd_slider_label.grid_forget()
        txt_label.grid_forget()
        txt_button.grid_forget()
        txt_entry.grid_forget()
        graph_frame.grid_forget()
        txt_label.grid(row=3, column=0)
        txt_button.grid(row=4, columnspan=2)
        txt_entry.grid(row=3, column=1)
        canvas.get_tk_widget().pack_forget()
        canvas._tkcanvas.pack_forget()
        toolbar.pack_forget()


#  Initialising variables
min_duration.trace("w", update_sliders)
max_duration.trace("w", update_sliders)
mean_duration.trace("w", update_sliders)
sd_duration.trace("w", update_sliders)
distribution = Tki.StringVar()
distribution.set("Uniform")
distribution.trace("w", update_distribution)
text = Tki.StringVar()


class Timer(object):
    def __init__(self):
        menu_frame.place_forget()
        settings_button.pack_forget()
        game_frame.place(relx=0.5, rely=0.5, anchor="center")
        next()
        self.display = Tki.Label(game_frame,
                                 text="",
                                 font=("Arial", 24))
        self.display.pack(anchor="s")
        if min_duration.get() == max_duration.get():
            self.duration = min_duration.get()
        elif distribution.get() == "Uniform":
            self.duration = random.uniform(min_duration.get(),
                                           max_duration.get())
        else:
            self.duration = stats.truncnorm.rvs(min_duration.get() -
                                                mean_duration.get(),
                                                max_duration.get() -
                                                mean_duration.get(),
                                                loc=mean_duration.get(),
                                                scale=sd_duration.get())
        self.start_time = time.time()
        self.update_timer()
        root.update_idletasks()
        root.update()

    def update_timer(self):
        now = time.time() - self.start_time
        if now <= self.duration and clues:
            self.display.configure(text="%.2f" % (now))
            root.after(10, self.update_timer)
        else:
            start_button.pack_forget()
            reset_button.pack(side="left")
            settings_button.pack(anchor="nw")
            self.display.pack_forget()
            game_frame.place_forget()
            menu()
            if not clues:
                tkMB.showinfo("Oops...",
                              "Boom!!!\n"
                              "The game ended because you ran out of clues.")
            else:
                tkMB.showinfo("Time up",
                              "Boom!!!\n"
                              "The game ended after %.2f seconds."
                              % (self.duration))
                if not continue_button.winfo_ismapped():
                    continue_button.pack(side="right")


def play():
    #  start the timer and play a round.
    timer = Timer()
    if timer:
        pass


def next():
    #  show the next clues.
    clue = clues.pop()
    clue_label.configure(text=clue)


def start():
    #  the command that runs when you press the start button.
    reset()
    play()


def reset():
    # reset clues and menu
    reset_clues()
    reset_menu()


def reset_menu():
    #  show the home screen
    start_button.pack(side="left")
    continue_button.pack_forget()
    reset_button.pack_forget()


def reset_clues():
    #  reread and shuffle the clues
    file = open("clues.txt", "r")
    lines = file.read().split("\n")
    file.close()
    random.shuffle(lines)
    global clues
    clues = [line for line in lines if line]


def menu():
    #  show the main menu.
    settings_frame.place_forget()
    settings_button.pack(anchor="nw")
    menu_button.pack_forget()
    menu_frame.place(relx=0.5, rely=0.5, anchor="center")


def settings():
    #  Show the settings menu.
    menu_frame.place_forget()
    menu_button.pack(anchor="nw")
    settings_button.pack_forget()
    settings_frame.place(relx=0.5, rely=0.5, anchor="center")


def clear_clues():
    #  Clear the local clues file.
    file = open("clues.txt", "w")
    file.write("")
    file.close()
    reset_clues()


# Frames
#  Settings widgets
distribution_label = Tki.Label(settings_frame,
                               text="Distribution:")
distribution_label.grid(row=0, column=0)
distribution_menu = Tki.OptionMenu(settings_frame,
                                   distribution,
                                   "Uniform",
                                   "Normal")
distribution_menu.grid(row=0, column=1, sticky="W")
menu_button = Tki.Button(root,
                         text="Main Menu",
                         command=menu)
min_slider = Tki.Scale(settings_frame,
                       variable=min_duration,
                       orient="horizontal",
                       from_=0,
                       resolution=10,
                       to_=200,
                       length=200
                       )
min_slider.grid(row=1, column=1, sticky="W")
min_slider_label = Tki.Label(settings_frame,
                             text="Minimum duration:")
min_slider_label.grid(row=1, column=0, sticky="S")
max_slider = Tki.Scale(settings_frame,
                       variable=max_duration,
                       orient="horizontal",
                       from_=min_duration.get(),
                       to_=min_duration.get() + 200,
                       resolution=10,
                       length=200)
max_slider.grid(row=2, column=1, sticky="W")
max_slider_label = Tki.Label(settings_frame,
                             text="Maximum duration:")
max_slider_label.grid(row=2, column=0, sticky="S")
mean_slider = Tki.Scale(settings_frame,
                        variable=mean_duration,
                        orient="horizontal",
                        from_=0,
                        to_=200,
                        resolution=10,
                        length=200
                        )
mean_slider_label = Tki.Label(settings_frame,
                              text="Mean duration:")
sd_slider = Tki.Scale(settings_frame,
                      variable=sd_duration,
                      orient="horizontal",
                      from_=0.1,
                      to_=10,
                      resolution=0.1,
                      length=200)
sd_slider_label = Tki.Label(settings_frame,
                            text="Standard deviation:")
txt_label = Tki.Label(settings_frame, text="New clue:")
txt_label.grid(row=3, column=0)
txt_entry = Tki.Entry(settings_frame,
                      textvariable=text,
                      relief="raised",
                      width=15)
txt_entry.insert(0, "Insert clue here...")
txt_entry.grid(row=3, column=1)
txt_button = Tki.Button(settings_frame,
                        text="Clear all saved clues",
                        command=clear_clues)
txt_button.grid(row=4, columnspan=2)
txt_entry.bind("<Return>", update_text)
#  Game widgets
clue_label = Tki.Label(game_frame, text="", font=("Arial", 36))
clue_label.pack(anchor="n")
next_button = Tki.Button(game_frame,
                         text="Next",
                         command=next,
                         font=("Arial", 12))
next_button.pack(anchor="center")
#  Menu widgets
start_button = Tki.Button(menu_frame,
                          text="Start",
                          command=start,
                          font=("Arial", 36, "bold"))
start_button.pack(side="left")
continue_button = Tki.Button(menu_frame,
                             text="Continue",
                             command=play,
                             font=("Arial", 36, "bold")
                             )
quit_image = Image.open("quit.png").resize((root.winfo_screenwidth() / 50,
                                            root.winfo_screenheight() / 50),
                                           Image.ANTIALIAS)
quit_PI = ImageTk.PhotoImage(quit_image)
quit_button = Tki.Button(root, image=quit_PI, command=root.destroy)
quit_button.pack(anchor="ne")
reset_button = Tki.Button(menu_frame,
                          text="Reset",
                          command=reset,
                          font=("Arial", 36, "bold"))
settings_button = Tki.Button(root,
                             text="Settings",
                             command=settings)
settings_button.pack(anchor="nw")

reset()
menu()
root.mainloop()
